<?php

class Logger {

    private $filePath = './logs.txt';   

    private function log($text = "", $type = "LOG") {
        return date('d M Y H:i:s') . " $type: $text \n";
    }

    public function info_log($text = "") {
        $file = fopen($this->filePath, "a");
        
        fwrite($file, $this->log($text, "INFO"));
    }

    public function error_log($text = "") {
        $file = fopen($this->filePath, "a");
        
        fwrite($file, $this->log($text, "ERROR"));
    }
}