<?php

function json_array(string $name, array $array): string
{
    return '{"' . $name . '":' . json_encode($array) . '}';
}

function json_object(string $name, object $object): string
{
    return '{"' . $name . '":' . json_encode($object) . '}';
}

function check_request(string $requestType, string $expectedRequest): bool
{
    if ($requestType != $expectedRequest) {
        http_response_code(405);
        return false;
    } else return true;
}

function validate_create_user_request(array $data): bool
{
    var_dump(sizeof($data));
    switch ($data) {
        case sizeof($data) < 3:
            return false;
        case !isset($data['name']):
        case !isset($data['image']):
        case !isset($data['password']):
            http_response_code(400);
            echo 'No all required parameters passed.';
            return false;
        default:
            return true;
    }
}

function validate_create_task_request(array $data): bool
{
    switch ($data) {
        case sizeof($data) < 5:
        case !isset($data['userId']):
        case !isset($data['assignedUserId']):
        case !isset($data['title']):
        case !isset($data['content']):
        case !isset($data['deadLine']):
            http_response_code(400);
            echo 'No all required parameters passed.';
            return false;
        default:
            return true;
    }
}