<?php

header("Content-type: application/json");

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_conn.php';
require_once $root . '/database/db_create.php';
require_once $root . '/database/db_drop.php';
require_once $root . '/database/db_select.php';
require_once $root . '/database/create_tables.php';
require_once $root . '/api/utils.php';

if (!check_request($_SERVER['REQUEST_METHOD'], "GET"))
    return;

use_db($conn, $dbName);

if (isset($_GET['id'])) {
    echo json_object("User", get_user_by_id($conn, intval($_GET['id'])));
    return;
}

echo json_array("Users", get_users($conn));
