<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_conn.php';
require_once $root . '/database/db_create.php';
require_once $root . '/database/db_drop.php';
require_once $root . '/database/db_select.php';
require_once $root . '/database/create_tables.php';
require_once $root . '/api/utils.php';

if (!check_request($_SERVER['REQUEST_METHOD'], "POST"))
    return;

if(!validate_create_user_request($_POST))
    return;

use_db($conn, $dbName);

$user = new DbUser(
    $_POST['name'],
    $_POST['image'],
    md5($_POST['password']),
    md5(date("h:i:sa"))
);

UserHandler::insert_user($conn, $user);

