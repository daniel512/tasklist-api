<?php

header("Content-type: application/json");

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_conn.php';
require_once $root . '/database/db_create.php';
require_once $root . '/database/db_drop.php';
require_once $root . '/database/db_select.php';
require_once $root . '/database/create_tables.php';
require_once $root . '/api/utils.php';

use_db($conn, $dbName);

if(isset($_GET['id'])) {  
    echo json_object("Task", get_task_by_id($conn, intval($_GET['id'])));
    return;
}

echo json_array("Tasks", get_tasks($conn));