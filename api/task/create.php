<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_conn.php';
require_once $root . '/database/db_create.php';
require_once $root . '/database/db_drop.php';
require_once $root . '/database/db_select.php';
require_once $root . '/database/create_tables.php';
require_once $root . '/api/utils.php';

if (!check_request($_SERVER['REQUEST_METHOD'], "POST"))
    return;

if (!validate_create_task_request($_POST))
    return;

use_db($conn, $dbName);

$task = new DbTask(
    $_POST['userId'],
    $_POST['assignedUserId'],
    $_POST['title'],
    $_POST['content'],
    $_POST['deadLine']
);

TaskHandler::insert_task($conn, $task);

