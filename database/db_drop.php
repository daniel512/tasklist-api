<?php

require_once 'db_conn.php';
require_once 'db_utils.php';

function drop_db(mysqli $conn, string $name)
{
    $sql = "DROP DATABASE $name";
    execute($conn, $sql);
}
