<?php

require_once 'tables/users.php';
require_once 'tables/tasks.php';

function create_tables(mysqli $conn)
{
    UserHandler::create_table($conn);
    TaskHandler::create_tasks($conn);
}

function insert_user(mysqli $conn, User $user)
{
    UserHandler::insert_user($conn, $user);
}

function insert_task(mysqli $conn, DbTask $task)
{
    TaskHandler::insert_task($conn, $task);
}

function get_users(mysqli $conn): array
{
    return UserHandler::all($conn);
}

function get_user_by_id(mysqli $conn, int $id): User
{
    return UserHandler::by_id($conn, $id);
}

function get_tasks(mysqli $conn): array
{
    return TaskHandler::all($conn);
}

function get_task_by_id(mysqli $conn, int $id): Task
{
    return TaskHandler::by_id($conn, $id);
}