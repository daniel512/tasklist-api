<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_conn.php';
require_once $root . '/database/db_create.php';
require_once $root . '/database/db_drop.php';
require_once $root . '/database/db_select.php';
require_once $root . '/database/create_tables.php';

drop_db($conn, $dbName);
create_db($conn, $dbName);
use_db($conn, $dbName);
create_tables($conn);

seed_db($conn, $root);
$x = get_users($conn);

echo json_encode($x);

$conn->close();