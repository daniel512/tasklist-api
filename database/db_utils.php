<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/Logger.php';
require_once $root . '/database/db_conn.php';
require_once $root . '/database/db_create.php';
require_once $root . '/database/db_drop.php';
require_once $root . '/database/db_select.php';
require_once $root . '/database/create_tables.php';

function execute(mysqli $conn, string $query)
{
    $logger = new Logger();
    $result = $conn->query($query);

    if ($result === TRUE) {
        $logger->info_log("Query executed successfully");
    } else {
        $logger->error_log($conn->error);
        $logger->error_log($query);
    }

    return $result;
}

function seed_db(mysqli $conn, string $rootUrl)
{
    $imageUrl = 'http://clearftp.cba.pl/tasklist_api/images/cat';

    for ($i = 1; $i < 4; $i++) {
        $u1 = new DbUser (
            "daniel",
            $imageUrl . (($i % 3) + 1) . '.png',
            "password2",
            md5("apikey" . $i)
        );

        insert_user($conn, $u1);
        $u1->id = 1;
        $t1 = new DbTask($i, $i, text_generator(rand(3, 7)), text_generator(120), date("Y-m-d H:m:s"));
        insert_task($conn, $t1);
    }
}

function text_generator(int $wordsCount): string
{
    $text = "";
    $words = explode(" ", "team that are causing deep offence and pain to members people should feel about the conflict between");

    for ($i = 0; $i < $wordsCount; $i++) {
        $text .= $words[rand(0, sizeof($words) - 1)] . ' ';
    }

    return $text;
}
