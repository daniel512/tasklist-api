<?php

require_once 'db_conn.php';
require_once 'db_utils.php';

function use_db(mysqli $conn, string $name)
{
    $sql = "USE $name \n";
    execute($conn, $sql);
}
