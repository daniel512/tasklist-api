<?php

require_once 'db_conn.php';
require_once 'db_utils.php';

function create_db(mysqli $conn, string $name)
{
    $sql = "CREATE DATABASE $name";
    execute($conn, $sql);
}
