<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_utils.php';
require_once $root . '/models/Task.php';
require_once $root . '/database/create_tables.php';

class TaskHandler
{

    public static function create_tasks(mysqli $conn)
    {
        $sql = "CREATE TABLE Tasks (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            user_id int(6) UNSIGNED NOT NULL,        
            assigned_user_id int(6) NOT NULL,
            title VARCHAR(100) NOT NULL,
            content VARCHAR(512) NULL,
            created_at DATETIME NOT NULL,
            dead_line DATETIME NOT NULL,
            done BOOL DEFAULT FALSE,
            CONSTRAINT FK_user_id FOREIGN KEY (user_id) REFERENCES Users(id)
            )";
        execute($conn, $sql);
    }

    public static function insert_task(mysqli $conn, DbTask $task)
    {
        $sql = "INSERT INTO Tasks(user_id, assigned_user_id, title, content, created_at, dead_line)
                VALUES (                
                    $task->userId,
                    $task->assignedUserId,
                    '$task->title',
                    '$task->content',
                    NOW(),
                    '$task->dead_line'
                )
        ";
        execute($conn, $sql);
    }

    public static function all(mysqli $conn): array
    {
        $sql = "SELECT t.id as t_id, t.title, t.content, u.id as user_id, au.id as a_user_id, t.created_at, t.dead_line, t.done
        FROM Tasks t
        INNER JOIN Users u ON t.user_id = u.id
        INNER JOIN Users au ON t.assigned_user_id = au.id
        ";

        $result = execute($conn, $sql);

        $data = $result->fetch_all(MYSQLI_ASSOC);
        $tasks = array();

        foreach ($data as $t) {
            $task = new Task(
                UserHandler::by_id($conn, $t['user_id']),
                UserHandler::by_id($conn, $t['a_user_id']),
                $t['title'],
                $t['content'],
                $t['created_at'],
                $t['dead_line'],
                $t['done']
            );

            $task->id = $t['t_id'];

            array_push($tasks, $task);
        }

        return $tasks;
    }

    public static function by_id(mysqli $conn, int $id): Task
    {
        $sql = "SELECT t.id as t_id, t.title, t.content, u.id as user_id, au.id as a_user_id, t.created_at, t.dead_line
        FROM Tasks t        
        INNER JOIN Users u ON t.user_id = u.id
        INNER JOIN Users au ON t.assigned_user_id = au.id   
        WHERE t.id = $id 
        ";

        $result = execute($conn, $sql);

        $data = $result->fetch_all(MYSQLI_ASSOC)[0];

        $task = new Task(
            UserHandler::by_id($conn, $data['user_id']),
            UserHandler::by_id($conn, $data['a_user_id']),
            $data['title'],
            $data['content'],
            $data['created_at'],
            $data['dead_line']
        );

        $task->id = $data['t_id'];
        return $task;
    }
}

