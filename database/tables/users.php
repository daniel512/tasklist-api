<?php

$root = $_SERVER['DOCUMENT_ROOT'];

require_once $root . '/database/db_utils.php';
require_once $root . '/models/User.php';

class UserHandler
{

    public static function create_table(mysqli $conn)
    {
        $sql = "CREATE TABLE Users (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(30) NOT NULL,
            image VARCHAR(100) NULL,
            password VARCHAR(100) NOT NULL,
            api_key VARCHAR(100) NOT NULL
            )";
        execute($conn, $sql);
    }

    public static function insert_user(mysqli $conn, User $user)
    {
        $sql = "INSERT INTO Users(name, image, password, api_key)
                VALUES (                
                    '$user->name',
                    '$user->image',
                    '$user->password',
                    '$user->apiKey'
                )
        ";
        execute($conn, $sql);
    }

    public static function all(mysqli $conn): array
    {
        $sql = "SELECT * FROM Users";

        $result = execute($conn, $sql);

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public static function by_id(mysqli $conn, int $id): User
    {
        $sql = "SELECT * FROM Users WHERE id = $id";

        $result = execute($conn, $sql);

        $data = $result->fetch_all(MYSQLI_ASSOC)[0];

        $user = new User($data['name'], $data['image']);
        $user->id = $data['id'];

        return $user;
    }
}
