<?php

class User
{

    public int $id;
    public string $name;
    public string $image;

    function __construct(string $name, string $image)
    {
        $this->name = $name;
        $this->image = $image;
    }
}

class DbUser extends User
{

    public string $password;
    public string $apiKey;

    function __construct(string $name, string $image, ?string $password, ?string $apiKey)
    {
        $this->name = $name;
        $this->image = $image;
        $this->password = $password;
        $this->apiKey = $apiKey;
    }
}