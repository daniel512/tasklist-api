<?php

require_once 'User.php';

class Task
{

    public int $id;
    public User $user;
    public User $assignedUser;
    public string $title;
    public string $content;
    public ?string $created_at;
    public ?string $dead_line;
    public bool $done = false;

    function __construct(
        User $user,
        User $assignedUser,
        string $title,
        string $content,
        ?string $created_at = null,
        ?string $dead_line = null,
        bool $done = false
    )
    {
        $this->user = $user;
        $this->assignedUser = $assignedUser;
        $this->title = $title;
        $this->content = $content;
        $this->created_at = $created_at;
        $this->dead_line = $dead_line;
        $this->done = $done;
    }
}

class DbTask
{

    public int $userId;
    public int $assignedUserId;
    public string $title;
    public string $content;
    public string $dead_line;

    function __construct(
        int $userId,
        int $assignedUserId,
        string $title,
        string $content,
        string $dead_line
    )
    {
        $this->userId = $userId;
        $this->assignedUserId = $assignedUserId;
        $this->title = $title;
        $this->content = $content;
        $this->dead_line = $dead_line;
    }
}
